import {Component, OnInit} from '@angular/core';
import {ApiService} from './services/api.service';
import {AddDataModel} from './models/addDataModel.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private apiService: ApiService) {
  }

  public names: string[] = [];

  public name = '';

  ngOnInit(): void {
    console.log('init');
  }

  public testApi(): void {
    this.apiService.apiTest().subscribe((response) => {
      alert(response.content);
    });
  }

  public writeToDatabase(): void {
    if (this.name === '') {
      return;
    }
    const addData: AddDataModel = {
      name: this.name,
      id: 666
    };
    this.apiService.addData(addData).subscribe((response) => {
      if (response === 1) {
        alert('Post to database, successful');
      }
    });
  }

  public readDatabase(): void {
    this.apiService.readData().subscribe((response) => {
      console.log(response);
      this.names = response;
    });
  }
}
