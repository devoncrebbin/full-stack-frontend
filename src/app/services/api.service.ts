import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AddDataModel} from '../models/addDataModel.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) {
  }

  testApiEndpoint = 'test-api';
  readDataEndpoint = 'test-database';
  addDataEndpoint = 'add-data';

  private getDefaultHeaders(): { headers: HttpHeaders; } {
    const httpHeaders: HttpHeaders = new HttpHeaders();
    return {headers: httpHeaders};
  }

  public apiTest(): Observable<any> {
    return this.httpClient.get('http://localhost:8080/' + this.testApiEndpoint, this.getDefaultHeaders());
  }

  public readData(): Observable<any> {
    return this.httpClient.get('http://localhost:8080/' + this.readDataEndpoint, this.getDefaultHeaders());
  }

  public addData(addData: AddDataModel): Observable<any> {
    const addDataParams = `?name=${addData.name}&id=${addData.id}`;
    return this.httpClient.get('http://localhost:8080/' + this.addDataEndpoint + addDataParams, this.getDefaultHeaders());
  }
}
